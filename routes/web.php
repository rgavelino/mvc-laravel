<?php


Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>['auth']],function(){
    Route::resource('users','UserController');
    Route::resource('roles','RoleController');
});

// Route::group(['middleware'=>['auth']],function(){
//     Route::resource('clientes','ClientesController');
//     Route::resource('roles','RoleController');
// });

Route::group(['prefix'=>'clientes'],function(){
    Route::get('/show','ClientesController@show')->middleware('auth')->name('clientes');
    Route::get('/create','ClientesController@create')->middleware('auth')->name('clientes');
    Route::get('/edit','ClientesController@edit')->middleware('auth')->name('clientes');
    Route::get('/index','ClientesController@index')->middleware('auth')->name('clientes');
});

Route::group(['prefix'=>'produtos'], function(){
    Route::get('/listar', 'ProdutosController@listar')->middleware('auth')->name('produtos');
});


Route::get('/avisos', function () {
    return view('avisos',['nome'=>'Rodrigo','mostrar' => true,
                'avisos' =>[['id'=> 1,'texto' => 'Aviso 1'], 
                            ['id' => 2,'texto'=>'Aviso 2']]     
    ]);
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
