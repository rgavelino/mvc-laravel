@extends ('layouts.externo')
@section('title','Quadro de Avisos da Empresa')
@section('sidebar')
    @parent
    <p>^^Barra superior adicionada do Layout pai/m&atilde;e^^</p>
@endsection

@section('content')
    <p>Quadro de Avisos da Empresa</p>
    <br>
<p>Ol&aacute;, {{$nome}}! Veja abaixo os avisos de hoje:</p>
@if($mostrar)
    Mostrando aviso

@foreach ($avisos as $aviso)
    <p>Aviso {{ $aviso['id'] }} : {{ $aviso['texto'] }}</p>
@endforeach

<?php
echo 'Exemplo com PHP puro.';
foreach($avisos as $aviso){
echo"<p>Aviso{$aviso['id']}:{$aviso['texto']}</p>";
}
?>
@else
Escondendo aviso
@endif

@endsection
