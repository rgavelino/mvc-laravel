<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Clientes extends Model
{
    protected $fillable = [
        'id',
        'nome', 
        'endereco', 
        'email', 
        'telefone'
    ];
    protected $table = 'Clientes';
        
    public function vendas(){
        return $this->hasMany(Vendas::class,'cliente_id');
    }
}

