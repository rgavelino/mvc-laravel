<?php

namespace App\Http\Controllers;

use App\Clientes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClientesController extends Controller
{

    private $qdtePag = 5;

    public function __construct() {
         $this->middleware('auth');
    }
    
    public function show($id){
        $cliente = Clientes::find($id);
        return view('clientes.show', compact('cliente'));
    }

    public function index( Request $request){
        $data = Clientes::orderBy('id', 'DESC')->paginate($this->qdtePag);
        return view('clientes.index', compact('data'))->with('i', ($request->input('page', 1)-1)*$this->qdtePag);
    }

    public function create(){
        return view('clientes.create');
    }

    public function store(Request $request){
        $this->validade($request, ['nome' => 'required',
                                   'email' => 'required|email'
                                   ]);
        $input = $request->all();    
        $user = Clientes::create($input);  
        return redirect()->route('clientes.index')-with('success', 'Cliente gravado com sucesso.');                         

    }

    public function edit($id){
        $cliente = Clientes::find($id);
        return view ('clientes.edit', compact('cliente'));
    }
    public function update(Request $request, $id){
        $this->validade($request, ['nome' => 'required',
                                   'email' => 'required|email'
                                   ]);
        $cliente = Clientes::find($id);
        $input = $request->all();
        $cliente->update($input);
        return redirect()->route('clientes.index')->with('success','Cliente atualizado com sucesso!');
    }
    public function destroy($id){
        Clientes::find($id)->delete();
        return redirect()->route('clientes.index')-with('success', 'Cliente removido com sucesso.');
    }
    
}
