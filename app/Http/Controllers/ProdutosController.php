<?php

namespace App\Http\Controllers;

use App\Produtos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProdutosController extends Controller
{
    public function listar(){
        $produtos = Produtos::all();

        return view('produtos.listar',['produtos'=>$produtos]);
    }
}
